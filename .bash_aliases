# .bash_aliases

##################
#    Aliases     #
##################
alias poule="git pull"
alias cd..="cd .."
alias l="ls -al"
alias ll="ls -all"
alias lp="ls -p"
alias h=history
# the "kp" alias ("que pasa"), in honor of tony p.
alias kp="ps auxwww"

alias logout='exit'

alias reload='source ~/.bash_profile'
alias reload_prompt='source ~/.bashrc'


alias la='ls -la'
alias tree="find . -print | sed -e 's;[^/]*/;|____;g;s;____|; |;g'"
