#!/bin/bash

dir=$(pwd)
ln -sf $dir/.bashrc ~/.bashrc 
ln -sf $dir/.bash_aliases ~/.bash_aliases 
ln -sf $dir/.bash_profile ~/.bash_profile 
ln -sf $dir/.vimrc ~/.vimrc 
ln -sf $dir/.vim ~/.vim 
source ~/.bashrc

