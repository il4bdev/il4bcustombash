# .bash_profile
# System-wide .profile for sh(1)

# To be placed in /etc/profile or ~/.bash_profile

# include .bashrc if it exists
if [ -f ~/.bashrc ]; then
	source ~/.bashrc
fi
