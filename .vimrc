" .vimrc

" Colorize syntax
colorscheme monokai
filetype plugin indent on
syntax on

" Show num lines
"set number

" Show current cmd line
set showcmd

" Auto-indent
set autoindent

" No comptability with VI
set nocompatible

" History size
set history=100

" Show match
set showmatch

" Highlight search results
set hlsearch

" Show live search results
set incsearch

if has("autocmd")
	filetype plugin indent on
	autocmd FileType text setlocal textwidth=78
	autocmd BufReadPost *
				\ if line("'\"") > 0 && line("'\"") <= line("$") |
				\   exe "normal g`\"" |
				\ endif
endif

" Shift round
set shiftround

" Ignore case when searching
set ignorecase

" Send more characters for redraws
set ttyfast

" Enable mouse use in all modes
set mouse=a

" Set this to the name of your terminal that supports mouse codes.
" Must be one of: xterm, xterm2, netterm, dec, jsbterm, pterm
set ttymouse=xterm2

" Detect file type
filetype on

" Pathogen
execute pathogen#infect()
call pathogen#helptags()
